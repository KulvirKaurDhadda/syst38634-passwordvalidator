package password;
/*
 * @author Kulvir Kaur Dhadda
 * student id - 991539865
 *
 *
 **/




public class PasswordValidator {

	/*
	 * The password validator validates these and only these requirements:
	    A password must have at least 8 character
	    A password must contain at least two digit
	 */
	
	public static final int MIN_LENGTH = 8;
	public static final int MIN_DIGIT_COUNT = 2;
	
	public static boolean hasValidCaseChars(String password)
	{
		return password != null && password.matches(".[A-Z]+.") && password.matches(".[a-z]+.");
	}

	public static boolean validLength(String password) {
		if(password.length() >= MIN_LENGTH && password != null) 
			return true;
		else
			return false;
	}
	
	public static boolean validDigitCount(String password) {
		int count = 0;
		for(int i = 0; i < password.length(); i++) {
			if (Character.isDigit(password.charAt(i)))
				count++;
		}
		
		return count >= MIN_DIGIT_COUNT ? true : false;
	}
	

}
