package password;
/*
 * author kulvir kaur dhadda
 * student id - 991539865
 * */
import static org.junit.Assert.*;

import org.junit.Test;

public class passwordValidatorTest {
	
	@Test
	public void testHasValidCaseCharsRegular()
	{
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("Harmeet"));
	}
	
	@Test
	public void testHasValidCaseCharsException()
	{
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("12345"));
	}
	
	@Test
	public void testHasValidCaseCharsExceptionEmpty()
	{
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars(""));
	}
	
	@Test
	public void testHasValidCaseCharsExceptionNull()
	{
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars(null));
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryOutUpper()
	{
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("A"));
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryOutLower()
	{
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("a"));
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryIn()
	{
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("Aa"));
	}

	@Test
	public void testValidLengthRegular () {
		assertTrue("Invalid password", PasswordValidator.validLength("something") == true);
	}
	
	@Test(expected = NullPointerException.class)
	public void testValidLengthException() {
		assertTrue("password not valid", PasswordValidator.validLength(null) !=false);
	}

	@Test
	public void testValidLengthBoundryIn() {
		assertTrue("password not valid", PasswordValidator.validLength("kulvirdh") == true);
	}
	
	@Test
	public void testValidLengthBoundryOut() {
		assertTrue("password not valid", PasswordValidator.validLength("kulvird") != true);
	}
	
	@Test
	public void testValidDigitCountRegular() {
		assertTrue("Password is not valid", PasswordValidator.validDigitCount("kulvirdh123")==true);
	}
	
	@Test (expected = NullPointerException.class)
	public void testValidDigitCountException() {
		assertTrue("Password is not valid", PasswordValidator.validDigitCount(null) != false);
	}
	
	@Test
	public void testValidDigitCountBoundryIn() {
		assertTrue("Password is not valid", PasswordValidator.validDigitCount("kulvirdh12"));
	}
	
	@Test
	public void testValidDigitCountBoundryOut() {
		assertTrue("Password is not valid", PasswordValidator.validDigitCount("kulvirdh1") == false);
	}
	

	
}
